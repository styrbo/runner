﻿using System;
using System.Collections.Generic;

public static class Console
{
    private struct Message
    {
        public string message;
    }

    public static Dictionary<string, Func<object, object>> Commands { get; private set; }

    private static List<Message> Messages;

    public static void Write(string message)
    {
        Messages.Add(new Message() { message = message });
    }

    public static void RegisterCommand(Func<object,object> command, string cmd)
    {
        if (Commands == null) Commands = new Dictionary<string, Func<object, object>>();

        Commands.Add(cmd, command);
    }

    public static object RunCommand(string cmd, object arg) => Commands[cmd]?.Invoke(arg);
}

