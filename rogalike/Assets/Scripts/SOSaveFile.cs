﻿using UnityEngine;
using Game.Artifact;

[CreateAssetMenu(menuName = "Game/Save")]
[System.Serializable]
public class SOSaveFile : ScriptableObject
{
    public SOArtifactData[] PlayerArtifacts;
}