﻿using UnityEngine;
using Unity.Entities;
using Unity.Mathematics;

namespace Test
{
    public class Test : MonoBehaviour
    {
        private void Start()
        {
            var manager = World.Active.EntityManager;
            var entity = manager.CreateEntity(typeof(Component));

            manager.SetComponentData(entity, new Component { Position = new float3(1, 1, 1) });

        }
    }
}