﻿using UnityEngine;
using Unity.Entities;

namespace Test
{
    public class System : ComponentSystem
    {
        protected override void OnUpdate()
        {
            Entities.ForEach((ref Component component) => {

                component.Position += 1;
            
            });
        }

    }
}