﻿using System;
using Unity.Entities;
using Unity.Mathematics;

namespace Test
{
    [Serializable]
    public struct Component : IComponentData
    {
        public float3 Position;
    }

}