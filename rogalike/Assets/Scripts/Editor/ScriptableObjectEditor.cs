﻿using System;
using System.Linq;
using System.Reflection;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEngine.UIElements;

[CustomEditor(typeof(ScriptableObject), true)]
class ScriptableObjectEditor : Editor
{
    Dictionary<FieldInfo, Type[]> _parseResult;
    ScriptableObject _target => target as ScriptableObject;

    public override VisualElement CreateInspectorGUI()
    {
        UpdateData();
        return base.CreateInspectorGUI();
    }

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        if (_parseResult != null)
        {
            foreach (var element in _parseResult)
                DrawInterface(element.Key, element.Value);
        }
    }

    /// <summary>
    /// обновляет информацию о объекте и его интерфейсов (для производительности не рекомендуется использовать каждый кадр, особенно в больших проектах!)
    /// </summary>
    void UpdateData()
    {
        _parseResult = new Dictionary<FieldInfo, Type[]>();

        var interfaceArray = _target.GetType().GetFields().Where((FieldInfo info) =>
        {
            if (info.FieldType.IsInterface == false)
                return false;

            return true;
        });

        foreach (var _interface in interfaceArray)
        {
            var types = ParseProgect(_interface.FieldType);

            _parseResult.Add(_interface, types);
        }

        EditorUtility.SetDirty(target);
    }

    /// <summary>
    /// ищет в сборке все классы которые наследуют интерфейс
    /// </summary>
    /// <param name="interfaceFinding">интерфейс который ищет метод</param>
    /// <param name="assembly">сборка в которой происходит поиск</param>
    /// <returns></returns>
    Type[] ParseProgect(Type interfaceFinding, string assembly = "Assembly-CSharp")
    {
        return Assembly.Load(assembly).GetTypes().Where((Type t) => t.GetInterface(interfaceFinding.Name) != null ? true : false).ToArray();
    }

    /// <summary>
    /// преобразует массив Type в массив имён этих типов
    /// </summary>
    /// <param name="types"></param>
    /// <returns></returns>
    string[] TypesToNames(Type[] types)
    {
        string[] names = new string[types.Length];

        for (int i = 0; i < types.Length; i++)
        {
            names[i] = types[i].Name;
        }
        return names;
    }

    /// <summary>
    /// возращяет позицию элемента в массиве
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="value"></param>
    /// <param name="array"></param>
    /// <param name="defoulthIndex">если array равен null или елемента нету в массиве, то функция возраящет defoulthIndex</param>
    /// <returns></returns>
    int IndexInArray<T>(T value, T[] array, int defoulthIndex = 0)
    {
        if (value == null)
            return defoulthIndex;

        for (int i = 0; i < array.Length; i++)
        {
            if (value.Equals(array[i]) == true)
                return i;
        }
        return defoulthIndex;
    }

    /// <summary>
    /// ресует интерфейс
    /// </summary>
    /// <param name="info">поле интерфейса</param>
    /// <param name="types">все классы которые наследуют данный интерфейс</param>
    void DrawInterface(FieldInfo info, Type[] types)
    {
        var typesName = TypesToNames(types);
        var fieldValue = info.GetValue(_target)?.GetType().Name;
        var index = IndexInArray(fieldValue, typesName);

        var newIndex = EditorGUILayout.Popup(index, typesName);

        info.SetValue(_target, Activator.CreateInstance(types[newIndex]) );
    }
}
