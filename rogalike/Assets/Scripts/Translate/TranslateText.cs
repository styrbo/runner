﻿using UnityEngine;
using UnityEngine.Events;

public class TranslateText : MonoBehaviour
{
    public string Key;

    public TextChanged OnChangedLanguage;

    private void Awake()
    {
      OnChangedLanguage?.Invoke(Translate.GetTranslate(Key));

      Translate.OnLanguageChanged += (string lang) => OnChangedLanguage?.Invoke(Translate.GetTranslate(Key));
    }

    [System.Serializable]
    public class TextChanged : UnityEvent<string> { }
}