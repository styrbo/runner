﻿using System.Collections.Generic;
using UnityEngine;
using System.Xml;
using System;

/// <summary>
/// всё что связвано с переводом для игры
/// </summary>
public class Translate
{
    /// <summary>
    /// версия ассета
    /// </summary>
    public const string version = "0.2";

    /// <summary>
    /// все переводы для всех языков
    /// </summary>
    public Dictionary<string, Dictionary<string, string>> Translates { get; private set; }

    /// <summary>
    /// язык который используется в данный момент
    /// </summary>
    public static string UsedLanguage
    {
        get => usedLanguage;
        set
        {
            usedLanguage = value;

            OnLanguageChanged?.Invoke(value);
        }
    }
    static string usedLanguage;

    /// <summary>
    /// срабатывает как только поменялся язык
    /// </summary>
    public static event Action<string> OnLanguageChanged;

    static Translate translate;

    /// <summary>
    /// получить сыллку на экземпляр класса
    /// </summary>
    /// <returns></returns>
    public static Translate Get
    {
        get
        {
            if (translate != null)
                return translate;

            translate = new Translate();
            return translate;
        }
    }

    /// <summary>
    /// загружает все переводы
    /// </summary>
    public void LoadData()
    {

        XmlDocument document = new XmlDocument();
        Translates = new Dictionary<string, Dictionary<string, string>>();

        document.Load(Application.streamingAssetsPath + "/Translate.xml");

        for (int i = 0; i < document.ChildNodes[1].ChildNodes.Count; i++)//i не 0 ибо первый элемент это метаданные
        {
            var language_node = document.ChildNodes[1].ChildNodes[i];
            var cur_language_directory = new Dictionary<string, string>();

            if (language_node.NodeType == XmlNodeType.Comment)// спасибо встроенному парсеру xml что тоже кушает коменты)
                continue;

            foreach (XmlNode key_node in language_node.ChildNodes)
            {

                if (key_node.NodeType == XmlNodeType.Comment)//та же история
                    continue;

                cur_language_directory.Add(key_node.Attributes[0].Value, key_node.InnerText);
            }

            Translates.Add(language_node.Attributes[0].Value, cur_language_directory);
        }
    }

    /// <summary>
    /// получает текст на используемом языке
    /// </summary>
    /// <param name="translateKey">ключ текста</param>
    /// <param name="SafeMode">проверять ли на различные ошибки </param>
    /// <returns></returns>
    public static string GetTranslate(string translateKey,bool SafeMode = false)
    {
        if (SafeMode)
        {
            //проверка на дурочка
            if (translate == null || translate.Translates == null)
            {
                Debug.LogError("Скрипт переводчика не инциализирован!");
                return null;
            }
        }

        return translate.Translates[UsedLanguage][translateKey];

    }

}
