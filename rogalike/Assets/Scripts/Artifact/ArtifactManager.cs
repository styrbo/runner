﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game.Artifact
{
    [DefaultExecutionOrder(1)]
    public class ArtifactManager : MonoBehaviour
    {
        List<Artifact> _artifacts;

        Player _player;

        private void Awake()
        {
            _player = GetComponent<Player>();
        }

        public void ApplayArtifacts()
        {
            for (int i = 0; i < _artifacts.Count; i++)
                ApplayArtifact(_artifacts[i]);
        }

        void ApplayArtifact(Artifact artifact)
        {
            var data = artifact.Data;

            if (data.ReplaceCharectiristic == true)
            {

                _player.MaxHp = CharactiristicsParse(_player.MaxHp, data.MaxHP);
                _player.Heal(CharactiristicsParse(_player.HP, data.MaxHP));
                _player.Speed = CharactiristicsParse(_player.Speed, data.Speed);
            }

            float CharactiristicsParse(float value, CharacteristicsSetting setting)
            {
                if (setting.Replace == false)
                    return value;

                switch (setting.AddMode)
                {
                    case EditCharacteristicsMode.plus:
                        value += setting.Count;
                        break;
                    case EditCharacteristicsMode.minus:
                        value -= setting.Count;
                        break;
                    case EditCharacteristicsMode.multiplay:
                        value *= setting.Count;
                        break;
                    case EditCharacteristicsMode.division:
                        value /= setting.Count;
                        break;
                }

                return value;
            };

        }

        public void AddArtifact(Artifact data) => _artifacts.Add(data);

        public void LoadArtifacts(SOArtifactData[] artifacts)
        {
            _artifacts = new List<Artifact>();

            for (int i = 0; i < artifacts.Length; i++)
                _artifacts.Add(new Artifact(artifacts[i]));
        }
        public SOArtifactData[] SaveArtifacts()
        {
            var array = new SOArtifactData[_artifacts.Count];

            for (int i = 0; i < _artifacts.Count; i++)
                array[i] = _artifacts[i].Data;

            return array;
        }

        public void DisplayArtifact()
        {

        }
    }
}