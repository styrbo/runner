﻿using System;
using UnityEngine;

namespace Game.Artifact
{
    [CreateAssetMenu(menuName = "Game/Item/Artifact", fileName = "new Artifact")]
    public class SOArtifactData : ScriptableObject
    {
        public Sprite Sprite;
        public string NameKey;
        public string DescriptionKey;
        [Header("изменение характиристик")]
        public bool ReplaceCharectiristic;

        public CharacteristicsSetting MaxHP;
        public CharacteristicsSetting HP;
        public CharacteristicsSetting Speed;
        [Header("всё что ниже пока что не работает")]
        public CharacteristicsSetting Size;

        [Header("защита")]
        public bool ReplaceResistance;

        public CharacteristicsSetting PiercingResistanse;
        public CharacteristicsSetting CuttingResistanse;
        public CharacteristicsSetting СrushingResistanse;

        public CharacteristicsSetting ExposionResistanse;
        public bool CanExplosionResistanse;

        public CharacteristicsSetting StunResistanseMaxProcent;
        public CharacteristicsSetting StunResistanseTime;
        public bool CanStunResistanse;

        public CharacteristicsSetting PoisonResistanseMaxProcent;
        public CharacteristicsSetting PoisonResistanse;
        public CharacteristicsSetting PoisonResistanseTime;
        public bool CanPoisonResistanse;

        public CharacteristicsSetting FireResistanseTime;
        public bool CanFireResistanse;

        public CharacteristicsSetting FrosenResistanseTime;
        public bool CanFrozenResistanse;

        [Header("урон")]
        public bool ReplaceDamage;

        public CharacteristicsSetting PiercingDamage;
        public CharacteristicsSetting CuttingDamage;
        public CharacteristicsSetting СrushingDamage;

        public CharacteristicsSetting ExposionDamage;
        public bool CanExplosionDamage;

        public CharacteristicsSetting StunDamageMaxProcent;
        public CharacteristicsSetting StunDamageTime;
        public bool CanStunDamage;

        public CharacteristicsSetting PoisonDamageMaxProcent;
        public CharacteristicsSetting PoisonDamage;
        public CharacteristicsSetting PoisonDamageTime;
        public bool CanPoisonDamage;

        public CharacteristicsSetting FireDamageTime;
        public bool CanFireDamage;

        public CharacteristicsSetting FrosenDamageTime;
        public bool CanFrozenDamage;
    }

    [Serializable]
    public struct CharacteristicsSetting
    {
        public bool Replace;
        public float Count;
        public EditCharacteristicsMode AddMode;
        public bool CanStack;
    }

    [Serializable]
    public enum EditCharacteristicsMode
    {
        plus,
        minus,
        multiplay,
        division
    }
}