﻿using UnityEngine;
using Game.Entity;

namespace Game.Weapon.Bullet
{
    public class Bullet : MonoBehaviour
    {
        public Entity.Entity Sender;
        public Vector2 Direction;
        public float Speed;

        private void FixedUpdate()
        {
            transform.Translate(Direction * Speed * Time.fixedDeltaTime);
        }

        private void OnCollisionEnter2D(Collision2D collision)
        {
            var entity = collision.gameObject.GetComponent<EntityBase>();

            Debug.Log('c');

            if (entity != null)
            {
                Debug.Log("SEND");
                entity.TakeDamage(Sender.DamageInfo);
            }
            Destroy(gameObject);
        }
    }
}
