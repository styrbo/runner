﻿using UnityEngine;

namespace Game.Weapon.Bullet
{
    [CreateAssetMenu(menuName = "Game/Weapon/BulletPatern", fileName = "new Bullet Patern")]
    public class SOBulletPatern : ScriptableObject
    {
        public float FireRate;
        public BulletInfo[] Bullets;
    }

    [System.Serializable]
    public struct BulletInfo
    {
        public GameObject Prefab;
        public Vector2 Position;
        public Quaternion Rotation;
        public Vector2 Direction;
        public float Speed;
    }
}