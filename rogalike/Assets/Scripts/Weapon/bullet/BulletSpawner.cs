﻿using UnityEngine;

namespace Game.Weapon.Bullet
{
    public class BulletSpawner : MonoBehaviour
    {
        public SOBulletPatern Patern;

        public bool CanShot { get; private set; }

        private float _ShotDelay;
        private Entity.Entity target;

        public void Shot()
        {
            if (CanShot == true)
            {
                foreach (BulletInfo info in Patern.Bullets)
                    CreateBullet(info);

                _ShotDelay = Patern.FireRate;
                CanShot = false;
            }
        }

        void CreateBullet(BulletInfo info)
        {
            var obj = Instantiate(info.Prefab,(Vector2)transform.position + info.Position,info.Rotation) as GameObject;
            var bullet = obj.GetComponent<Bullet>();

            bullet.Sender = target;
            bullet.Direction = info.Direction;
            bullet.Speed = info.Speed;
        }

        private void Awake()
        {
            target = GetComponent<Entity.Entity>();
        }

        private void Update()
        {
            if (_ShotDelay < 0)
                CanShot = true;

            _ShotDelay -= Time.deltaTime;
        }

    }
}