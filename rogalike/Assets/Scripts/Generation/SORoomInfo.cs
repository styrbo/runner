﻿using UnityEngine;

[CreateAssetMenu(menuName = "Game/Room",fileName = "new Room")]
public class SORoomInfo : ScriptableObject
{
    public Vector2Int RoomSize;
    public Vector2Int[] QuitsPosition;
    public GameObject Room;
}

