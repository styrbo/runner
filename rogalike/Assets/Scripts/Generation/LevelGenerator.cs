﻿using System;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;

public class LevelGenerator : MonoBehaviour
{
    private class RoomInfo
    {
        public RoomInfo[] Up;
        public RoomInfo[] Left;
        public RoomInfo[] Down;
        public RoomInfo[] Rignt;

        public Vector2Int startSize;
        public Vector2Int endSize;
    }

    public int RoomCount;
    public int Seed;
    public System.Random Random;

    private SORoomInfo[] _rooms;

    private void Awake()
    {
        Random = new System.Random(Seed);

        Console.RegisterCommand(Command_GenerateLevel, "new level");

        LoadRooms();
    }

    private void LoadRooms()
    {
        Console.Write("load rooms...");
        _rooms = Resources.LoadAll<SORoomInfo>("SO/Rooms");
        Console.Write($"load rooms COMPLITED, rooms count: {_rooms.Length}");
    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(1)) GenerateLevel();
    }

    private void GenerateLevel()
    {
        var timer = System.Diagnostics.Stopwatch.StartNew();
        Console.Write("start gc...");
        GC.Collect();
        Console.Write("start generation level...");
        var map_pos = CreateMap();
        Console.Write("pre map OK");
        var room_info = ToRoomInfo(map_pos);
        Console.Write("generate roomInfo OK");
        //JoinRoom(ref room_info);
        Console.Write("joint room Nan");
        CreateRooms(room_info);
        Console.Write("rooms create OK");
        Console.Write($"level generate SUCCESS for time:{timer.ElapsedMilliseconds}");
    }

    private Vector2Int[] CreateMap()
    {
        var vacantPlaces = new HashSet<Vector2Int>();
        var rooms = new List<RoomInfo>();

        vacantPlaces.Add(Vector2Int.zero);
        vacantPlaces.Add(Vector2Int.up);

        for (int i = 0; i < RoomCount; i++)
        {
            var position = GetSide(Random.Next(4)) + vacantPlaces.Last();

            while (vacantPlaces.Contains(position) || position.y <= 0)
            {
                position = GetSide(Random.Next(4)) + vacantPlaces.ToArray()[Random.Next(vacantPlaces.Count)];
            }

            vacantPlaces.Add(position);
        }

        return vacantPlaces.ToArray();
    }

    private RoomInfo[] ToRoomInfo(Vector2Int[] positions)
    {
        RoomInfo[] result = new RoomInfo[positions.Length];

        for (int i = 0; i < result.Length; i++) { result[i] = new RoomInfo() { startSize = positions[i] }; }

        foreach (var item in result)
        {
            item.Up = result.Where((RoomInfo info) => (item.startSize + Vector2Int.up) == info.startSize).ToArray();
            item.Left = result.Where((RoomInfo info) => (item.startSize + Vector2Int.left) == info.startSize).ToArray();
            item.Down = result.Where((RoomInfo info) => (item.startSize + Vector2Int.down) == info.startSize).ToArray();
            item.Rignt = result.Where((RoomInfo info) => (item.startSize + Vector2Int.right) == info.startSize).ToArray();
        }

        return result;
    }

    private void JoinRoom(ref RoomInfo[] rooms)//TODO: доделать генерацию больших комнат
    {
        var list = rooms.ToList();

        for (int i = 0; i < list.Count; i++)
        {
            if (list[i].startSize == Vector2Int.zero)
                continue;

            var defoulth = list[i];

            //foreach(var pos in list[i].Up)
                //list[i].endSize 

        }
    }

    private void CreateRooms(RoomInfo[] infous)
    {
        for (int i = 0; i < transform.childCount; i++)
        {
            Destroy(transform.GetChild(i));
        }

        for (int i = 0; i < infous.Length; i++)
        {
            var suitableRoom = _rooms.Where((SORoomInfo info) =>
            {
                foreach (var quit in info.QuitsPosition)
                {
                    //infous[i].Up.Length + infous[i].Rignt.Length + infous[i].Down.Length + infous[i].Left.Length;

                    if (infous[i].Up[0] == null && quit == Vector2Int.up)
                        return false;
                    if (infous[i].Rignt[0] == null && quit == Vector2Int.right)
                        return false;
                    if (infous[i].Down[0] == null && quit == Vector2Int.down)
                        return false;
                    if (infous[i].Left[0] == null && quit == Vector2Int.left)
                        return false;
                }
                return true;
            }).ToArray();

            CreateRoom(suitableRoom[Random.Next(suitableRoom.Count())]);
        }
    }

    private void CreateRoom(SORoomInfo info)
    {
        foreach (var maps in info.Room.transform.GetChild(0))
        {
            
        }
    }

    private Vector2Int GetSide(int side)
    {
        switch (side)
        {
            case 0:
                return Vector2Int.up;
            case 1:
                return Vector2Int.right;
            case 2:
                return Vector2Int.down;
            case 3:
                return Vector2Int.left;

        }

        return Vector2Int.zero;
    }

    private object Command_GenerateLevel(object arg)
    {
        if (arg != null)
        {
            Seed = (int)arg;
            Random = new System.Random(Seed);
        }

        GenerateLevel();

        return null;
    }

}