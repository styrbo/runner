﻿using UnityEngine;
using Game.Entity;
using Game.Artifact;

[RequireComponent(typeof(ArtifactManager))]
[DefaultExecutionOrder(0)]
public class Player : Entity
{
    [SerializeField] private Joystick _moveJoystick = null;
    [SerializeField] private Joystick _rotateJoystick;

    protected override void Awake()
    {
        base.Awake();
    }

    protected override void Update()
    {
        base.Update();

        var dir = _moveJoystick.Direction;

        if (dir != Vector2.zero)
            Move(dir);

        if (Input.GetMouseButtonDown(1))
        {
            Debug.Log("start work");
            GetComponent<ArtifactManager>().LoadArtifacts(save.PlayerArtifacts);
            GetComponent<ArtifactManager>().ApplayArtifacts();
        }
    }
    public SOSaveFile save;

}