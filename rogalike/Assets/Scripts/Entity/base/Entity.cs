﻿using System.Collections.Generic;
using UnityEngine;

namespace Game.Entity
{
    public abstract class Entity : EntityBase
    {
        [Header("умножение урона")]
        /// <summary>
        /// количество урона 
        /// </summary>
        public DamageInfo DamageInfo;

        List<Effect> _effects;

        public virtual void Move(Vector2 direction)
        {
            transform.Translate(direction * Speed * Time.deltaTime);
        }

        public void RemoveEffect()
        {

        }

        public void ClearEffects()
        {
            _effects.ForEach((effect) => effect.Clear());
            _effects.Clear();
        }

        public void AddEffect(params Effect[] effects)
        {
            foreach (var item in effects)
                item.Add();

            _effects.AddRange(effects);
        }

    }
}