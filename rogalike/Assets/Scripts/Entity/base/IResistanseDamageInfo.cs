﻿public interface IResistanseDamageInfo
{
    float Piercing { get; set; }
    float Cutting { get; set; }
    float Сrushing { get; set; }
    float Explosion { get; set; }
    float Stun { get; set; }
    float Poison { get; set; }
    float Fire { get; set; }
    float Frosen { get; set; }
}

