﻿using UnityEngine;
using UnityEngine.Events;

namespace Game.Entity
{
    public abstract class EntityBase : MonoBehaviour, IResistanseDamageInfo
    {
        [SerializeField] private float _hp = 100;
        [SerializeField] private float _speed = 1;

        public float MaxHp;
        public float HP
        {
            get => _hp;
            protected set
            {
                OnHPChanget?.Invoke(value);

                if (value <= MaxHp)
                    _hp = value;
                else
                    _hp = MaxHp;
            }
        }
        public float Speed
        {
            get => _speed * speedMultiplay;
            set
            {
                speedMultiplay = value;
                unStunMultiplaySpeed = value;
            }
        }
        private float speedMultiplay;
        private float unStunMultiplaySpeed;

        #region Damage Procent Multiplay
        [Header("Resistance Procent Multiplay")]

        [SerializeField] private float _piercing = 1;
        [SerializeField] private float _cutting = 1;
        [SerializeField] private float _crushing = 1;
        [SerializeField] private float _explosion = 1;
        [SerializeField] private float _stun = 1;
        [SerializeField] private float _poison = 1;
        [SerializeField] private float _fire = 1;
        [SerializeField] private float _frosen = 1;

        public float Piercing { get => _piercing; set => _piercing = value; }
        public float Cutting { get => _cutting; set => _cutting = value; }
        public float Сrushing { get => _crushing; set => _crushing = value; }
        public float Explosion { get => _explosion; set => _explosion = value; }
        public float Stun { get => _stun; set => _stun = value; }
        public float Poison { get => _poison; set => _poison = value; }
        public float Fire { get => _fire; set => _fire = value; }
        public float Frosen { get => _frosen; set => _frosen = value; }
        #endregion

        private float? bleedingTime;
        private float bleedingPower;

        private float? stunTime;
        private float stunProcent;

        private float? poisonTime;
        private float poisonProcent;
        private float poisonDamage;

        private float? fireTime;
        private float fireDamage;

        private float? frosenTime;

        #region events
        protected event UnityAction<float> OnTakeDamage;
        protected event UnityAction<float> OnHPChanget;
        protected event UnityAction<float> OnHeal;

        protected event UnityAction<float> OnBleeding;
        protected event UnityAction OnBleedingEnd;

        protected event UnityAction OnStun;
        protected event UnityAction OnStunEnd;

        protected event UnityAction<float> OnFire;
        protected event UnityAction OnFireEnd;

        protected event UnityAction<float> OnPoison;
        protected event UnityAction OnPoisonEnd;

        protected event UnityAction OnFrosen;
        protected event UnityAction OnFrosenEnd;
        #endregion

        /// <summary>
        /// выдаёт сущности урон
        /// </summary>
        /// <param name="info"></param>
        public void TakeDamage(DamageInfo info)
        {
            float count = 0;

            var deltaPiercingDamage = info.PiercingDamage * Piercing;
            GiveBleadingEffect(deltaPiercingDamage, .25f * deltaPiercingDamage);
            count += deltaPiercingDamage;

            var deltaCuttingDamage = info.CuttingDamage * Cutting;
            GiveBleadingEffect(.25f * deltaCuttingDamage, deltaCuttingDamage);
            count += deltaCuttingDamage;

            var deltaCrushingDamage = info.СrushingDamage * Сrushing;
            GiveBleadingEffect(deltaCrushingDamage * 2, 0);
            count += deltaCrushingDamage / 4;

            count += _explosion * info.ExposionDamage;

            GiveStunEffect(info.StunProcent, info.StunTime);
            GivePoisonEffect(info.PoisonTime, info.PoisonProcent, info.PoisonTime);
            GiveFireEffect(info.FireDamage, info.FireTime);
            GiveFrosenEffect(info.FrozenTime);

            HP -= count;
            OnTakeDamage?.Invoke(count);
        }

        /// <summary>
        /// подхиливает сущность
        /// </summary>
        /// <param name="Count"></param>
        public void Heal(float Count)
        {
            HP += Count;
            OnHeal?.Invoke(Count);
        }

        #region Effects

        /// <summary>
        /// выдаёт эффект кровотечение сущности
        /// </summary>
        /// <param name="power"></param>
        /// <param name="time"></param>
        public virtual void GiveBleadingEffect(float power, float time)
        {
            if (bleedingTime == null)
                bleedingTime = new float();

            bleedingTime += time;
            bleedingPower += power;
        }

        /// <summary>
        /// выдаёт эффект замедление сущности
        /// </summary>
        /// <param name="procent"></param>
        /// <param name="time"></param>
        public virtual void GiveStunEffect(float procent, float time)
        {
            if (stunTime == null)
                stunTime = new float();

            stunTime = stunTime == 0 ? time * Stun : (stunTime + (procent * Stun)) / 2;
            stunProcent = stunProcent == 0 ? procent * Stun : (stunProcent + (procent * Stun)) / 2;   //TODO: сделать ограничение чтобы не становилось больше 1, а то начнётся херня
            speedMultiplay *= procent;

        }

        /// <summary>
        /// выдаёт эффект отравления сущности
        /// </summary>
        /// <param name="power"></param>
        /// <param name="procent"></param>
        /// <param name="time"></param>
        public virtual void GivePoisonEffect(float power, float procent, float time)
        {
            if (poisonTime == null)
                poisonTime = new float();

            poisonTime = poisonTime == 0 ? time * Poison : (poisonTime + (time * Poison)) / 2;
            if (poisonTime != 0)
            {
                poisonProcent = poisonProcent == 0 ? procent * Poison : (poisonProcent + (procent * Poison)) / 2; //TODO: сделать ограничение чтобы не становилось больше 1, а то начнётся херня
                poisonDamage = poisonDamage == 0 ? power * Poison : (poisonDamage + (power * Poison)) / 2;
            }
        }

        /// <summary>
        /// выдаёт эффект горения сущности
        /// </summary>
        /// <param name="power"></param>
        /// <param name="time"></param>
        public virtual void GiveFireEffect(float power, float time)
        {
            if (fireTime == null)
                fireTime = new float();

            fireTime += time * Fire;
            fireDamage += power * Fire;
        }

        /// <summary>
        /// выдаёт эффект заморозки сущности
        /// </summary>
        /// <param name="time"></param>
        public virtual void GiveFrosenEffect(float time)
        {
            if (frosenTime == null)
                frosenTime = new float();

            speedMultiplay = 0;

            frosenTime = frosenTime == 0 ? time * Frosen : (frosenTime + (time * Frosen)) / 2;
        }
        #endregion

        protected virtual void Awake()
        {
            speedMultiplay = 1;
            unStunMultiplaySpeed = 1;
        }
        protected virtual void Update()
        {
            if (bleedingTime != null)
            {
                if (bleedingTime <= 0)
                {
                    OnBleedingEnd?.Invoke();
                    bleedingPower /= 10;
                    bleedingTime = null;
                }
                OnBleeding?.Invoke(bleedingPower * Time.deltaTime);
                HP -= bleedingPower * Time.deltaTime;
                bleedingTime -= Time.deltaTime;
            }
            if (stunTime != null)
            {
                if (stunTime <= 0)
                {
                    OnStunEnd?.Invoke();
                    Speed = unStunMultiplaySpeed;
                    stunTime = null;
                }
                OnStun?.Invoke();
                stunTime -= Time.deltaTime;
            }
            if (poisonTime != null)
            {
                if (poisonTime <= 0)
                {
                    OnPoisonEnd?.Invoke();
                    poisonTime = null;
                }
                if (HP - poisonDamage > HP * poisonProcent)
                {
                    HP -= poisonDamage * Time.deltaTime;
                    OnPoison?.Invoke(poisonDamage * Time.deltaTime);
                }
                poisonTime -= Time.deltaTime;
            }
            if (fireTime != null)
            {
                if (fireTime <= 0)
                {
                    OnFireEnd?.Invoke();
                    fireTime = null;
                }
                OnFire?.Invoke(fireDamage * Time.deltaTime);
                HP -= fireDamage * Time.deltaTime;
                fireTime -= Time.deltaTime;
            }
            if (frosenTime != null)
            {
                if (frosenTime <= 0)
                {
                    Speed = unStunMultiplaySpeed;
                    OnFrosen?.Invoke();
                    frosenTime = null;
                }
                OnFrosenEnd?.Invoke();
                frosenTime -= Time.deltaTime;
            }
        }   //TODO: поменять в куратину эти таймеры
    }
}