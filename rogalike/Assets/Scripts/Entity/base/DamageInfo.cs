﻿namespace Game
{
    [System.Serializable]
    public struct DamageInfo
    {
        public float PiercingDamage;
        public float CuttingDamage;
        public float СrushingDamage;

        public float ExposionDamage;

        public bool CanStun;        //TODO: засунуть эти булианы в EntityBase
        public float StunProcent;
        public float StunTime;

        public bool CanPoison;
        public float PoisonProcent;
        public float PoisonDamage;
        public float PoisonTime;

        public bool CanFire;
        public float FireDamage;
        public float FireTime;

        public bool CanFrozen;
        public float FrozenTime;
    }
}