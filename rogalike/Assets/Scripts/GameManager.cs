﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static GameManager Get { get; private set; }

    private void Awake()
    {
        Get = this;
    }

    public SOSaveFile UsedSave;
}
