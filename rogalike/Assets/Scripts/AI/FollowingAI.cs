﻿using System.Collections.Generic;
using UnityEngine;

namespace Game.AI
{
    public class FollowingAI : AI
    {
        protected override void Update()
        {
            base.Update();

            var targetVector = (Target.position - transform.position).normalized;

            rb.AddForce(targetVector * Speed);
        }
    }
}