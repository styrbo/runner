﻿using Game.Weapon.Bullet;
using UnityEngine;

namespace Game.AI
{
    [RequireComponent(typeof(Rigidbody2D),typeof(BulletSpawner))]
    public class AI : Entity.Entity
    {
        /// <summary>
        /// цель к которой будет стремиться сущность
        /// </summary>
        public Transform Target;

        protected Rigidbody2D rb;
        protected BulletSpawner weapon;

        protected override void Awake()
        {
            base.Awake();

            rb = GetComponent<Rigidbody2D>();
            weapon = GetComponent<BulletSpawner>();
        }

        public override void Move(Vector2 direction) => rb.AddForce(direction, ForceMode2D.Impulse);
    }
}