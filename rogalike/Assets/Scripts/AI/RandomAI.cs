﻿using System.Collections;
using UnityEngine;

namespace Game.AI
{
    public class RandomAI : AI
    {
        public float ImpulseTime;

        protected override void Awake()
        {
            base.Awake();

            StartCoroutine(ImpulseTimer());
        }

        IEnumerator ImpulseTimer()
        {
            while (true)
            {
                yield return new WaitForSeconds(ImpulseTime);

                var move_vector = new Vector2(Random.Range(-Speed, Speed), Random.Range(-Speed, Speed));
                Debug.Log("sdas");
                Move(move_vector);
                weapon.Shot();
            }
        }
    }
}