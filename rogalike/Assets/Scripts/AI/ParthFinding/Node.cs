﻿using UnityEngine;

namespace Game.AI
{
    public class Node
    {
        public Vector2Int Position;
        public Vector2Int TargetPosition;
        public Node PreviousNode;

        /// <summary>
        /// расстояние от старта до ноды
        /// </summary>
        public uint G;

        /// <summary>
        /// расстояние от ноды до цели
        /// </summary>
        public uint H;

        /// <summary>
        /// G + H
        /// </summary>
        public uint F;

        public Node(uint g, Vector2Int nodePosition, Vector2Int targetPositon, Node previousNode)
        {
            Position = nodePosition;
            TargetPosition = targetPositon;
            PreviousNode = previousNode;
            G = g;
            H = (uint) (Mathf.Abs(targetPositon.x - Position.x) + Mathf.Abs(targetPositon.y - Position.y));
            F = G + H;
        }

    }
}