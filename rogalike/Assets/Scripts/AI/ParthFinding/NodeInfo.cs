﻿using System;
using UnityEngine;

namespace Game.AI
{
    public class NodeInfo
    {
        public bool CanMove { get; private set; }

        public NodeInfo(bool canMove)
        {
            CanMove = canMove;
        }

    }
}
