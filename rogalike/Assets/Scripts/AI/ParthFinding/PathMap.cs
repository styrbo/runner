﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Game.AI
{
    public sealed class PathMap
    {
        public delegate bool CheckNodePosInObstacles(Vector2 position, Vector2 castSize, LayerMask CastLayer);

        public PathMap(Vector2 startPos, Vector2 endPos, Vector2Int GridCount)
        {
            _startPos = startPos;
            _endPos = endPos;
            _gridCount = GridCount;
        }

        public NodeInfo this[int x, int y]
        {
            get => _map[x, y];
        }
        public NodeInfo this[Vector2Int pos]
        {
            get => this[pos.x, pos.y];
        }

        public CheckNodePosInObstacles CheckObstacle;
        public LayerMask CastMask;

        NodeInfo[,] _map;
        Vector2Int _gridCount;
        Vector2 _startPos;
        Vector2 _endPos;
        Vector2 _castSize;

        public void BakeMap()
        {

            if (_gridCount.x <= 0 || _gridCount.y <= 0)
            {
                Debug.LogError("количество ячеек меньше или равен нулю");
                return;
            }

            _map = new NodeInfo[_gridCount.x, _gridCount.y];
            float xStep = StepSize(_startPos.x, _endPos.x, _gridCount.x);
            float yStep = StepSize(_startPos.y, _endPos.y, _gridCount.y);

            _castSize = new Vector2(xStep / 2, yStep / 2);

            for (int x = 0; x < _map.Length; x++)
            {
                for (int y = 0; y < _map.GetLength(1); y++)
                {
                    _map[x, y] = CreateNode(new Vector2(xStep * x, yStep * y));
                }
            }

        }

        NodeInfo CreateNode(Vector2 position)
        {
            return new NodeInfo((CheckObstacle ?? DefoulthCheckNodePosInObstacle).Invoke(position, _castSize, CastMask));
        }

        bool DefoulthCheckNodePosInObstacle(Vector2 pos, Vector2 size, LayerMask Castlayer)
        {
            return Physics2D.BoxCast(pos, size, 0, Vector2.zero, Castlayer, 10, CastMask);
        }

        float StepSize(float start, float end, float count) => (Mathf.Abs(end) - Mathf.Abs(start)) / count;

    }
}
