﻿using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.Diagnostics;

namespace Game.AI
{
    public class PathFinding : MonoBehaviour
    {
        public LayerMask SolidLayer;
        public Transform target;

        public List<Vector2Int> path;

        public List<Vector2Int> GetPath(Vector2Int from, Vector2Int to,PathMap targetMap)
        {
            var PathToTarget = new List<Vector2Int>();
            var CheakedNodes = new List<Node>();
            var WaltingNodes = new List<Node>(); //TODO: может в Query?

            if (from == to) return null;

            Node startNode = new Node(0, from, to, null);

            CheakedNodes.Add(startNode);
            WaltingNodes.AddRange(GetHeighbourNodes(startNode));

            while (WaltingNodes.Count > 0)
            {
                Node nodeToCheck = WaltingNodes.Where(x => x.F == WaltingNodes.Min(y => y.F)).FirstOrDefault();

                if (nodeToCheck.Position == to)
                    return CalculatingPathFromNode(nodeToCheck);

                var wallCheck = targetMap[nodeToCheck.Position].CanMove;

                WaltingNodes.Remove(nodeToCheck);

                if (wallCheck)
                {
                    CheakedNodes.Add(nodeToCheck);
                }
                else
                {
                    var sameNodes = CheakedNodes.Where(x => x.Position == nodeToCheck.Position).ToList();

                    if (!sameNodes.Any())
                    {
                        CheakedNodes.Add(nodeToCheck);
                        WaltingNodes.AddRange(GetHeighbourNodes(nodeToCheck));
                    }

                }



            }

            return PathToTarget;
        }

        private List<Vector2Int> CalculatingPathFromNode(Node node)
        {
            var path = new List<Vector2Int>();
            Node currentNode = node;

            while (currentNode.PreviousNode != null)
            {
                path.Add(currentNode.Position);
                currentNode = currentNode.PreviousNode;
            }

            return path;
        }

        private List<Node> GetHeighbourNodes(Node node)
        {
            var result = new List<Node>();

            result.Add(new Node(node.G + 1, new Vector2Int(node.Position.x + 1, node.Position.y), node.TargetPosition, node));
            result.Add(new Node(node.G + 1, new Vector2Int(node.Position.x, node.Position.y + 1), node.TargetPosition, node));
            result.Add(new Node(node.G + 1, new Vector2Int(node.Position.x - 1, node.Position.y), node.TargetPosition, node));
            result.Add(new Node(node.G + 1, new Vector2Int(node.Position.x, node.Position.y - 1), node.TargetPosition, node));

            return result;
        }

    }
    [CustomEditor(typeof(PathFinding))]
    class PathInspector : Editor
    {

        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            if (GUILayout.Button("Generate"))
            {
                var targ = target as PathFinding;

                var time = Stopwatch.StartNew();

                targ.path = targ.GetPath
                    (
                    new Vector2Int((int)targ.transform.position.x, (int)targ.transform.position.y),
                    new Vector2Int((int)targ.target.position.x, (int)targ.target.position.y),
                    null
                    );

                UnityEngine.Debug.Log(time.ElapsedMilliseconds);

            }

        }
    }
}
/*
                        for (int i = 0; i < sameNodes.Count; i++)
                        {
                            if (sameNodes[i].F > nodeToCheck.F)
                        }
*/